import Extension from '@ohos.application.ServiceExtensionAbility'
import rpc from '@ohos.rpc'

const REQUEST_VALUE = 1;

class StubTest extends rpc.RemoteObject {
    constructor(des) {
        super(des);
    }
    onRemoteRequest(code, data, reply, option) {
        console.log(`onRemoteRequest`);
        if (code === REQUEST_VALUE) {
            let optFir = data.readInt();
            let optSec = data.readInt();
            reply.writeInt(optFir + optSec);
            console.log(`onRemoteRequest: opt: ${optFir}, opt2: ${optSec}`);
        }
        return true;
    }
    queryLocalInterface(descriptor) {
        return null;
    }
    getInterfaceDescriptor() {
        return "";
    }
    sendRequest(code, data, reply, options) {
        return null;
    }
    getCallingPid() {
        return REQUEST_VALUE;
    }
    getCallingUid() {
        return REQUEST_VALUE;
    }
    attachLocalInterface(localInterface, descriptor){}
}

export default class ServiceExtAbility extends Extension {
    onCreate(want) {
        console.log(`onCreate, want: ${want.abilityName}`);
    }
    onRequest(want, startId) {
        console.log(`onRequest, want: ${want.abilityName}`);
    }
    onConnect(want) {
        console.log(`onConnect , want: ${want.abilityName}`);
        return new StubTest("test");
    }
    onDisconnect(want) {
        console.log(`onDisconnect, want: ${want.abilityName}`);
    }
    onDestroy() {
        console.log(`onDestroy`);
    }
}